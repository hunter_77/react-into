import React from "react";
import "./productsList.css";
import Product from "./Product/Product";

const ProductList = ({ title, products }) => {
  return (
    <div style={{margin: '10px'}}>
      <h1>{title}</h1>
      <div className="product" style={{ gap: "15px", flexWrap: "wrap" }}>
        {products &&
          products?.map((product) => (
            <Product key={product.id} product={product} />
          ))}
      </div>
    </div>
  );
};

export default ProductList;
